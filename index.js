const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user');
const recipeSchema = require('./models/recipe');

const app = express();

app.use(express.json());

mongoose.connect('mongodb://poli:password@mongoRecetas:27017/recipes?authSource=admin')
.then(() => console.log('conectado a Mongo'))
.catch((error) => console.error(error))


app.post('/new_user', (req,res) => {
    const user = userSchema(req.body);
    user.save()
    .then((data) => {
        console.log('Usuario agregado')
        res.json(data)
    })
    .catch((error) => res.send(error))
        console.log("error usuario")
})

app.post('/new_recipe', (req,res) => {
    const recipe = recipeSchema(req.body);
    recipe.save()
    .then((data) => {
        console.log('Receta agregada')
        res.json(data)
    })
    .catch((error) => res.send(error))
        console.log("error receta")
})


app.get('/recipes', (req,res) => {
    const {userId,recipeId} = req.body
    if (recipeId){
        recipeSchema
            .find({_id : recipeId})
            .then((data) => {
                console.log('Se lista la receta solicitada')
                res.json(data)
            })
            .catch((error)=>res.json({message: error}))
    } else {
        recipeSchema
            .find({userId : userId})
            .then((data) => {
                console.log('Se listan todas las recetas del usuario')
                res.json(data)
            })
            .catch((error)=>res.json({message: error}))
    }
})

app.get('/recipesbyingredient', (req,res) => {
    const {ingredients} = req.body
    const ingredientes = ingredients.map(a=>a.name)
    console.log(ingredientes)
    recipeSchema
        .find({$expr:{$setIsSubset: ["$ingredients.name",ingredientes]}})
        .then((data) => {
            console.log('Se listan las recetas solicitadas')
            res.json(data)
        })
        .catch((error)=>res.json({message: error}))

})

app.listen(3000, () => console.log("Esperando en puerto 3000..."))